// For simplicity, assume 
//      1. V = {0, 1, 2, ..., n-1}
//      2. Edges input as unordered pairs (from stdin)
//      3. The graph is an undirected graph
//      4. A (weighted) adjacency matrix representation to be used

#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <tuple>
#include <limits>

using namespace std;

#define INFINITY numeric_limits< int>::max();

using Edges=vector<vector<int>>;

//read edges in "edge-list" format 
auto getInputs( )
{ 
	unsigned long u, v, weight, source_node;
        
	Edges adj_matrix;
	
         cin>>source_node;

	 while (cin>>u>>v>>weight)
	 {
            auto order = max(max(adj_matrix.size(),u+1),v+1);
              
		 adj_matrix.resize(order);

                 for (auto& e : adj_matrix) // make matrix square
                    e.resize(order);

		 adj_matrix[u][v] = adj_matrix[v][u] = weight;
	  } 
	  
	  return make_tuple(source_node, adj_matrix);

}

auto deleteMin(vector<int> D, vector<bool> Q) // This takes $\Theta(n)$
{
int i = D.size()-1;
int min = -1;

while (i >= 0 and Q[i] == false)
 i--;

  min = i;


for(int j = 0; j < i; j++)
 if(Q[j] == true and D[j] < D[min])
   min = j;

return min;
}

void SSSP(const Edges& cost, unsigned src)
{
  
  auto n = cost.size();

  vector<int> dist(n); //"distance array" will be used as the priority queue
  vector<bool> in_queue(n,true); //All n nodes initially in priority queue  


  //inialize d[]
  for(auto& d: dist)
      d = INFINITY;

  dist[src] = 0; // Set distance to source


//iterate over all nodes
  for(unsigned i = 0; i < n ; i++) 
    {
	 auto v = deleteMin(dist, in_queue);

         //do other stuff
        in_queue(n,false);
        for(v=true)
        {
            if
        }

     }  	
      	
  cerr<<endl<<endl<<"Shortest Paths from node '"<<src<<"' are:"<<endl;
   
  for(unsigned i =0; i < n; i++)
  {
    cerr <<"["<< i<<"]   "<<dist[i]<< endl;
   }

 
	
	
}

int main()
{
  auto [source, edges] = getInputs();
  
  cerr<<endl<<"Input Cost Matrix: "<<endl;
  
  
  for(auto e: edges)
   {
    for(auto weight: e)
       cerr<< weight<<" ";
   
     cerr<<endl;
   } 


 
 SSSP(edges, source);
}



